from sys import path
from matplotlib.image import thumbnail
import matplotlib.pyplot as plt
import csv
import os
import numpy as np
from numpy.lib.function_base import append

avgs = {}
thruputs = {}

plt.grid()
for root, dirs, files in os.walk("../Tulokset"):
    for entry in files:
        
        if(entry.endswith(".csv")):
            with open(root+"/"+entry,'r') as csvfile:
                lines = csv.reader(csvfile, delimiter=',')
                next(lines)
                Values = []
                Values2 = []
                for row in lines:
                    Values.append(int(row[1]))
                    Values2.append(int(row[0]))
            # Take 2 std deviations from the dataset
            Values = np.array(Values)
            line_val = np.mean(Values.reshape(-1,14000),1)
            mean = np.mean(Values)
            std_dev = np.std(Values)
            distance_from_mean = abs(Values - mean)
            max_deviation = 2
            not_outlier = distance_from_mean < max_deviation * std_dev
            Values = Values[not_outlier]
            avgs[entry[15:len(entry)-4].replace("MicroK8s", "µK8s")] = np.round_(np.average(Values),2)
            thruputs[entry[15:len(entry)-4].replace("MicroK8s", "µK8s")] = np.round_(len(Values2)/((np.max(Values2)-np.min(Values2))/1000),2)
            # Plot line
            plt.rc('axes', axisbelow=True)
            plt.plot(np.array(range(1,len(line_val)+1))*14000,line_val, color = 'g',linewidth=1)
            plt.xlabel('Iteraatio')
            plt.ylabel('Vasteaika (ms)')
            plt.title(csvfile.name, fontsize = 16)
            plt.savefig(csvfile.name+ "_line.png",dpi=300,bbox_inches="tight")
            plt.close()
            plt.grid()
            # Make histogram
            Values,bins = np.histogram(Values,bins=50)
            width = 0.7 * (bins[1] - bins[0])
            center = (bins[:-1] + bins[1:]) / 2
            # Plot histogram
            plt.rc('axes', axisbelow=True)
            plt.bar(center,Values,align='center', color = 'g',width=width)
            plt.xlabel('Vasteaika (ms)')
            plt.title(csvfile.name, fontsize = 16)
            plt.savefig(csvfile.name+ "_histogram.png",dpi=300,bbox_inches="tight")
            plt.close()
            plt.grid()

avgs=dict(sorted(avgs.items(), key=lambda item: item[1]))
thruputs=dict(sorted(thruputs.items(), key=lambda item: item[1],reverse=True))
plt.rc('axes', axisbelow=True)
plt.bar(avgs.keys(),avgs.values(),color='g')
plt.title("Vasteajat", fontsize = 16)
plt.ylabel('Vasteajan keskiarvo (ms.)')
plt.xticks(rotation=-45,fontsize = 8,ha="left" ,rotation_mode="anchor")
plt.savefig("../Tulokset/vasteajat_koneittain.png",dpi=300,bbox_inches="tight")
plt.close()
plt.grid()

plt.rc('axes', axisbelow=True)
plt.bar(thruputs.keys(),thruputs.values(),color='g')
plt.title("Välityskyky", fontsize = 16)
plt.ylabel('Pyyntöä sekunnissa')
plt.xticks(rotation=-45,fontsize = 8,ha="left" ,rotation_mode="anchor")
plt.savefig("../Tulokset/pyynnöt_koneittain.png",dpi=300,bbox_inches="tight")
plt.close()
plt.grid()


# MicroK8s vs K3s
arr_k3s=[]
arr_uK8s=[]
for key in thruputs.keys():
    if "K3s" in key:
        arr_k3s.append(thruputs[key])
    if "K8s" in key:
        arr_uK8s.append(thruputs[key])

arr=[]
arr.append(np.average(arr_k3s))
arr.append(np.average(arr_uK8s))
names=[]
names.append("K3s")
names.append("MicroK8s")
arr[1] = arr[1]/arr[0]
arr[0]=1

plt.rc('axes', axisbelow=True)
plt.bar(names,arr,color='g')
plt.title("Kubernetes jakeluiden ero", fontsize = 16)
plt.ylabel('Suhteellinen välityskyky')
plt.yticks(np.arange(0, 1.1, 0.1))
plt.xticks(rotation=-45,fontsize = 8,ha="left" ,rotation_mode="anchor")
plt.savefig("../Tulokset/k3s_vs_microk8s.png",dpi=300,bbox_inches="tight")
plt.close()
plt.grid()
# Kube vs noKube
arr_nok=[]
arr_k=[]


for key in thruputs.keys():
    if "NATIVE" not in key:
        if "K" in key:
            arr_k.append(thruputs[key])
        else:
            arr_nok.append(thruputs[key])

arr=[]
arr.append(np.average(arr_nok))
arr.append(np.average(arr_k))
names=[]
names.append("Ei Kubernetesia")
names.append("Kubernetes asennettu")

arr[1] = arr[1]/arr[0]
arr[0]=1

plt.rc('axes', axisbelow=True)
plt.bar(names,arr,color='g')
plt.title("Kubernetesin aiheuttama välityskyvyn lasku (JVM koonti)", fontsize = 14)
plt.ylabel('Suhteellinen välityskyky')
plt.yticks(np.arange(0, 1.1, 0.1))
plt.xticks(rotation=-45,fontsize = 8,ha="left" ,rotation_mode="anchor")
plt.savefig("../Tulokset/kube_vs_nokube.png",dpi=300,bbox_inches="tight")
plt.close()

for key in avgs:
    print(key, 'avg time: ', avgs[key], " ms. , Throughput: ", thruputs[key], " req/s.")

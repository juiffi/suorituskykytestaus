![throughtput chart](/Tulokset/pyynnöt_koneittain.png)
# Suorituskykytestaus

Sisältää Quarkuksella toteutetun rajapintasovelluksen suorituskykytestit. Testit on suoritettu kolmella eri virtuaalipalvelimella (Hetzner CX11, CX21 ja CXP31). Palvelimilla oli 1, 2 ja 4 CPU:ta ja 2/4/8 Gigatavua keskusmuistia

Testien aikana tietokannanhallintajärjestelmä oli asennettu erilliselle palvelimelle ja JMeter sovellusta suorittava palvelin oli niinikään erillinen. Tavoitteena oli varmistaa, että testauksen kohteena on nimenomaan rajapintaa tarjoava palvelin, eikä tietokantapalvelin tai JMeteriä suorittava palvelin


Testeissä Quarkuksen JVM toteutus oli n. kaksi kertaa nopeampi vasteajoiltaan ja välityskyvyltään kuin GraalVM:llä natiiviksi käännetty sovellus.


Sovellusta ajettiin ensin ilman kubernetesta, ja sen jälkeen kahdella eri Kubernetes jakelulla (MicroK8s ja k3s). Minikubea oli tarkoitus testata, mutta portin avaaminen klusterista ulkomaailmaan ei onnistunut. 


Kubernetes asennus suoritettiin nk. single-node klusterina. Klusterille asennettiin deployment ja kuormanjako (MicroK8s käytti MetalLB:tä, k3s Rancherin Klipper kuormanjakoa).


Tuloksissa k3s oli pääsääntöisesti n. 4% nopeampi. Ilman Kubernetesia tehty suoritus oli n. 15% nopeampi kuin klusterin kanssa.

![throughtput chart](/Tulokset/k3s_vs_microk8s.png)
![throughtput chart](/Tulokset/kube_vs_nokube.png)
Testeissä suoritettiin 700 rinnakkaisella säikeellä GET pyyntöjä Quarkus sovellukselle, joka kutsui tietokantaa ja palautti sitten vastauksen. Silmukka toistetiin 3000 kertaa, yhteensä 2100000 kutsua/konfiguraatio


Histogrammeista ja vertailuista on poistettu yli 2 keskihajonnan etäisyydellä olevat tulokset, .cvs datassa se on mukana.

Viivadiagrammeissa 2,1 miljoonasta tuloksesta on otettu 150 pisteen keskiarvo ja piirretty kuvaaja niiden perusteella lukemisen helpottamiseksi.


Kuvaajat on piirreety python -kansiossa olevalla visualise -skriptillä.

Natiivikäännös alisuoriutui ilman Kubernetesia 8G koneella, sillä natiivi ei onnistunut hyödyntämään kaikkia cpu resursseja. Kubernetesilla ongelma kierrettiin asettamalla replikoiden määrä vastaamaan cpu ytimien määrää ja välissä oli kuormanjako (Klipper tai MetalLB).

Suorituskykytestauksen perusteella käyttöön otetaan CX11 palvelininstanssit ja JVM buildi, sillä vaikka Kubernetesin asentaminen vie resursseja Quarkusen suorittamiselta, häviö on pieni. JVM buildi on nopeampi kuin natiivi ja JVM buildin tekeminen vie paljon vähemmän laitteistoresursseja ja valmistuu siten nopeammin. Kylmäkäynnistyksen nopeus on natiivin etu, mutta sillä ei tässä käyttötapauksessa ole juuri väliä. 


Data saatavilla (pakattuna ~300Mt, purettuna ~5Gt) [Google Drivestä](https://drive.google.com/file/d/1v1jDDf6tpmBh0d_67c49_m62mkD60M-5/view?usp=sharing)





